#ifndef GunnerThread_h_
#define GunnerThread_h_

#include <cc++/thread.h>
#include <iostream>
#include "Robot.h"

	using namespace std;
	using namespace ost;

	class GunnerThread : public Thread {

		public:

			GunnerThread(Robot *_Rob);
			~GunnerThread(){ };

			void Init();
			void Radar(long double Distance, int ObjectType, long double Angle);
			void Info(long double GunAngle);
			void EnemyInfo(long double Level, int Team);
			void RotationReached();

			void SetMaxRotate(long double MaxRotate);
			void SetShotSpeed(long double ShotSpeed);
			void SetShotMinEnergy(long double ShotMinEnergy);
			void SetShotMaxEnergy(long double ShotMaxEnergy);
			void SetEnergyIncreaseSpeed(long double IncreaseSpeed);

			void SetDebug(long double Debug);

			virtual void run();

		private:
			Semaphore *Sem;
			Robot *Rob;

	};

#endif
