#ifndef RobotGoverner_h_
#define RobotGoverner_h_

#include <cc++/thread.h>
#include <iostream>
#include "Robot.h"

	using namespace std;
	using namespace ost;

	class RobotGoverner : public Thread {

		public:
			RobotGoverner(Robot *Rob);
			~RobotGoverner(){

			};

			void Init();
			void Warning(int WarningType, string Message);

			void SetStartEnergy(double StartEnergy);
			void SetMaxEnergy(double MaxEnergy);
			void SetEnergyLevels(double Levels);

			void SetDebug(double Debug);

			void SetTimeout(double Timeout);
			

			virtual void run();

		private:
			Semaphore *Sem;
			Robot *Rob;

	};

#endif
