#ifndef WallPoint_h_
#define WallPoint_h_

#include "Node.h"
#include <cc++/thread.h>
#include <iostream>

	using namespace std;
	using namespace ost;

	class WallPoint : public Node {

		public:
			WallPoint() : Node() {};
			WallPoint(long double _X, long double _Y, long double _Time = 0) : Node(), X(_X), Y(_Y), Time(_Time) {};
			WallPoint(WallPoint *wp) : Node() {
				X = wp->X;
				Y = wp->Y;
				Time = wp->Time;
			};
			void Print();
			~WallPoint() { };
			volatile long double X;
			volatile long double Y;
			long double Time;
	};

#endif
