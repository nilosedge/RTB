#ifndef MoverThread_h_
#define MoverThread_h_

#include <cc++/thread.h>
#include "Robot.h"
#include <math.h>
#include <iostream>
#include "WallPoint.h"

	using namespace std;
	using namespace ost;

	class MoverThread : public Thread {

		public:
			MoverThread(Robot *_Rob);
			~MoverThread(){

			};

			void Init();
			void Radar(long double Distance, int ObjectType, long double Angle);
			void Info(long double CurrentSpeed);
			void SetCoods(long double X, long double Y, long double Angle);
			void RotationReached();
			void Collision(int ObjectType, long double RealitiveAngle);

			void SetMaxRotate(double MaxRotate);
			void SetMaxAccel(double MaxAccel);
			void SetMinAccel(double MinAccel);
			void SetDebug(double Debug);
			void Exit();

			virtual void run();

		private:

			void AccelerateRobot(long double AccelAmount);
			void TurnRobot();

			Robot *Rob;
			Semaphore *Sem;

			int Counter;
			bool Exiting;
			bool Rotating;
			bool FirstRadar;

			volatile long double RadarDistance;
			volatile int RadarObjectType;
			volatile long double RadarAngle;

			volatile long double Direction;

			long double MaxRotateSpeed;
			long double DebugLevel;
			long double MaxAccelSpeed;
			long double MinAccelSpeed;

			int scale;
	};

#endif
