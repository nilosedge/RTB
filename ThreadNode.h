#ifndef ThreadNode_h_
#define ThreadNode_h_

#include "Node.h"
#include <cc++/thread.h>
#include <iostream>

	using namespace std;
	using namespace ost;

	class ThreadNode : public Node {

		public:
			ThreadNode(Thread *_thread) : Node(), thread(_thread), finished(false) {};
			void Print();
			~ThreadNode() { delete thread; };
			Thread *thread;
			volatile bool finished;

	};

#endif
