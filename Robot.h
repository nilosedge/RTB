#ifndef Robot_h_
#define Robot_h_

#include "MessageTypes.h"
#include "WallPoint.h"
#include "Queue.h"
#include <iostream>
#include <string>

#define PI 3.141592653589793238462643
	
	using namespace std;

	class Robot {

		public:

			Robot(string *_Color, string *_Name);
			~Robot();
			void Print();
			void Init();
			void Exit();
			void Dead();
			void SetOption(int Option, double Value);
			void SetEnergyLevel(double EnergyLevel);
			void GameStarts();
			void GameFinished();
			void Radar(long double Distance, int ObjectType, long double Angle);
			void Info(long double TimeSenceStart);
			void RotationReached(int WhatRotated);
			void RobotsLeft(int NumberOfEnemies);
			void SetDebug(double Debug);
			void SetSendingCoords(double ReportCoords);
			void SetLocation(long double x, long double y);
			WallPoint * GetLocation();

			void ClearPoints();

			bool AddWallPoint(WallPoint *wp);
			bool AddCookiePoint(WallPoint *wp);
			bool AddMinePoint(WallPoint *wp);
			Queue * GetWallPoints();
			Queue * GetCookiePoints();
			Queue * GetMinePoints();

			string * GetName();
			void SetName(string *_Name);
			string * GetColor();
			void SetColor(string *_Color);

			enum RobotStates {
				HUNTING=1,
				MAPPING=2,
				FLEEING=3
			};

			volatile int State;
			volatile bool Playing;
			volatile long double RadarDistance;
			volatile int RadarObjectType;
			volatile long double RadarAngle;

			volatile long double RobotTime;
			volatile int ClearedTime;
			volatile int LastClearedTime;
			volatile long double X;
			volatile long double Y;


		private:
			bool WallPointExists(WallPoint *wp);
			bool CookiePointExists(WallPoint *wp);
			bool MinePointExists(WallPoint *wp);

			bool Cleared;

			WallPoint *Location;

			Queue *WallPoints;
			Queue *CookiePoints;
			Queue *MinePoints;

			string *Color;
			string *Name;
			Semaphore *Sem;
			Semaphore *Loc;
	};

#endif
