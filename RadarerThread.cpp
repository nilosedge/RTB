#include "RadarerThread.h"

RadarerThread::RadarerThread(Robot *_Rob) {
	Sem = new Semaphore(1);
	Rob = _Rob;
	Rotating = false;
	Exiting = false;
}

void RadarerThread::Exit() {
	Exiting = true;
}

void RadarerThread::Init() {


}

void RadarerThread::SetMaxRotate(long double MaxRotate) {
	MaxRotateSpeed = MaxRotate;
	cerr << "MaxRotateSpeed: " << MaxRotateSpeed << endl;
}

void RadarerThread::RotationReached() {
	Rotating = false;
}

void RadarerThread::UpdateRadar(long double Distance, int ObjectType, long double Angle) {
	RadarDistance = Distance;
	RadarObjectType = ObjectType;
	RadarAngle = Angle;
}


void RadarerThread::SetDebug(long double Debug) {

}

void RadarerThread::RotateRadar(long double RotateAmount) {
	float WorkingAngle = RadarAngle;
	if(!Rotating) {
		Rotating = true;
		if((WorkingAngle + RotateAmount) > (PI * 2)) {
			WorkingAngle = (WorkingAngle + RotateAmount) - (PI * 2);
		} else {
			WorkingAngle += RotateAmount;
		}
		cout << "RotateTo 4 8 " << WorkingAngle << endl;
		//cout << "RotateAmount 1 2 " << WorkingAngle + 1 << endl;
		//cerr << "Rotating to: " << WorkingAngle << endl;
	}
}

void RadarerThread::run() {
	while(!Exiting) {
		while(Rob->Playing) {
			if(Rob->State == Rob->MAPPING) {
				RotateRadar(4);
			} else {

			}
			usleep(5);
		}
		while(!Rob->Playing) {
			usleep(5);
		}
	}
	usleep(5);
}
