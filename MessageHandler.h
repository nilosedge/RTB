#ifndef MessageHandler_h_
#define MessageHandler_h_

#include <cc++/thread.h>
#include <iostream>
#include "Queue.h"
#include "Robot.h"
#include "GunnerThread.h"
#include "MoverThread.h"
#include "RadarerThread.h"
#include "RobotGoverner.h"
#include <string>
#include <fstream>
#include "MessageTypes.h"

	using namespace std;
	using namespace ost;

	class MessageHandler : public Thread {

		public:
			MessageHandler(Robot *_Rob, GunnerThread *_Gunner, MoverThread *_Mover, RadarerThread *_Radarer, RobotGoverner *_Governer);
			~MessageHandler() {
				if(Sem) delete Sem;
			};

			void ParseMessage();

			virtual void run();
		
		private:
			Semaphore *Sem;
			string Message;
			ofstream *fout;
			Robot *Rob;
			GunnerThread *Gunner;
			MoverThread *Mover;
			RadarerThread *Radarer;
			RobotGoverner *Governer;
			int Game;
			int TimeToQuit;

	};

#endif
