#include <stdlib.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <unistd.h>
#include <X11/keysym.h>

#include <cc++/thread.h>
#include "Robot.h"
#include "Queue.h"
#include <iostream>

#ifndef XWindowThread_h_
#define XWindowThread_h_

	using namespace std;
	using namespace ost;

	class XWindowThread : public Thread {

		public:
			XWindowThread(Robot *_Rob);
			~XWindowThread() {};
			virtual void run();

			Display *dis; /* Display - Instance of an X server */

			Window window; /* Window ID */
			Window win; /* Window ID */

			XEvent report;
			XEvent watch;

			GC drawcx; /* Drawing graphics context */
			Colormap colormap;

			XColor Black,White,Green,Red,RobotC;
			int height;
			int width;
			int shift;
			int scale;

			Robot *Rob;
	};

#endif
