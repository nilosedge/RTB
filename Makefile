CC = c++
LIBS = -lccgnu2 -lpthread -ldl -L/usr/X11R6/lib -lX11
LIB = ThreadNode.o Node.o WallPoint.o Queue.o MessageHandler.o Robot.o MoverThread.o RadarerThread.o GunnerThread.o XWindowThread.o RobotGoverner.o
FLAGS = -Wall -pg -g -I/usr/include/cc++2

all: Main

%.o: %.cpp %.h
	$(CC) $(FLAGS) -c $<

Main: Main.cpp $(LIB)
	$(CC) $(FLAGS) -o Robot $(LIB) $(LIBS) $<

install: 
	cp Robot /usr/lib/realtimebattle/Robots/Robot.robot

clean:
	rm -fr $(LIB)
	rm -fr Main
	rm -fr Robot
	rm -fr *.out
	rm -fr *.s
