#include "Robot.h"


Robot::Robot(string *_Color, string *_Name) {
	Color = _Color;
	Name = _Name;
	State = MAPPING;
	WallPoints = new Queue(100000);
	CookiePoints = new Queue(100);
	MinePoints = new Queue(100);
	Playing = false;
	Sem = new Semaphore(1);
	Loc = new Semaphore(1);
}

void Robot::Radar(long double Distance, int ObjectType, long double Angle) {
	RadarDistance = Distance;
	RadarObjectType = ObjectType;
	RadarAngle = Angle;

}

void Robot::Info(long double TimeSenceStart) {
	RobotTime = TimeSenceStart;

	ClearedTime = (int)RobotTime;
	if(ClearedTime != LastClearedTime) {
		ClearPoints();
		LastClearedTime = ClearedTime;
	}

}

void Robot::ClearPoints() {
	Sem->wait();

	if(CookiePoints->CurrentSize > 0) {
		Queue *NewCookiePoints = new Queue(100);

		while(CookiePoints->CurrentSize > 0) {
			WallPoint *wp = (WallPoint *)CookiePoints->Pop();
			if((RobotTime - wp->Time) > 10) {
				delete wp;
			} else {
				NewCookiePoints->Push(wp);
			}
		}

		delete CookiePoints;
		CookiePoints = NewCookiePoints;
	}

	if(MinePoints->CurrentSize > 0) {
		Queue *NewMinePoints = new Queue(100);
		while(MinePoints->CurrentSize > 0) {
			WallPoint *wp = (WallPoint *)MinePoints->Pop();
			if((RobotTime - wp->Time) > 10) {
				delete wp;
			} else {
				NewMinePoints->Push(wp);
			}
		}
		delete MinePoints;
		MinePoints = NewMinePoints;
	}
/*
	if(WallPoints->CurrentSize > 0) {
		Queue *NewWallPoints = new Queue(100000);
		while(WallPoints->CurrentSize > 0) {
			WallPoint *wp = (WallPoint *)WallPoints->Pop();
			if((RobotTime - wp->Time) > 25) {
				delete wp;
			} else {
				NewWallPoints->Push(wp);
			}
		}
		delete WallPoints;
		WallPoints = NewWallPoints;
	}
*/
	Sem->post();
}

void Robot::SetEnergyLevel(double EnergyLevel) {


}

void Robot::RobotsLeft(int NumberOfEnemies) {


}

void Robot::RotationReached(int WhatRotated) {

}
Queue * Robot::GetCookiePoints() {
	Sem->wait();
	Queue *q = new Queue(CookiePoints->CurrentSize + 1);
	for(WallPoint *Work = (WallPoint *)CookiePoints->Head; Work != NULL; Work = (WallPoint *)Work->Next) {
		q->Push(new WallPoint(Work));
	}
	Sem->post();
	return q;
}

Queue * Robot::GetMinePoints() {
	Sem->wait();
	Queue *q = new Queue(MinePoints->CurrentSize + 1);
	for(WallPoint *Work = (WallPoint *)MinePoints->Head; Work != NULL; Work = (WallPoint *)Work->Next) {
		q->Push(new WallPoint(Work));
	}
	Sem->post();
	return q;
}

Queue * Robot::GetWallPoints() {
	Sem->wait();
	Queue *q = new Queue(WallPoints->CurrentSize + 1);
	for(WallPoint *Work = (WallPoint *)WallPoints->Head; Work != NULL; Work = (WallPoint *)Work->Next) {
		q->Push(new WallPoint(Work));
	}
	Sem->post();
	return q;
}

bool Robot::WallPointExists(WallPoint *wp) {
	for(WallPoint *Work = (WallPoint *)WallPoints->Head; Work != NULL; Work = (WallPoint *)Work->Next) {
		if(wp->X == Work->X && wp->Y == Work->Y) {
			Work->Time = RobotTime;
			return true;
		}
	}
	return false;
}

bool Robot::CookiePointExists(WallPoint *wp) {
	for(WallPoint *Work = (WallPoint *)CookiePoints->Head; Work != NULL; Work = (WallPoint *)Work->Next) {
		if(wp->X == Work->X && wp->Y == Work->Y) {
			Work->Time = RobotTime;
			return true;
		}
	}
	return false;
}

bool Robot::MinePointExists(WallPoint *wp) {
	for(WallPoint *Work = (WallPoint *)MinePoints->Head; Work != NULL; Work = (WallPoint *)Work->Next) {
		if(wp->X == Work->X && wp->Y == Work->Y) {
			Work->Time = RobotTime;
			return true;
		}
	}
	return false;
}

void Robot::SetLocation(long double x, long double y) {
	X = x;
	Y = y;
}

WallPoint * Robot::GetLocation() {
	return new WallPoint(X, Y, 0);
}


void Robot::Init() {

}

bool Robot::AddCookiePoint(WallPoint *wp) {
		Sem->wait();
		if(!CookiePointExists(wp)) {
			wp->Time = RobotTime;

			WallPoint *del = (WallPoint *)CookiePoints->Pop();
			delete del;

			CookiePoints->Push(wp);
			//wp->Print();
			//cerr << "WallPoints Size: " << WallPoints->CurrentSize << endl;
			Sem->post();
			return true;
		} else {
			Sem->post();
			return false;
		}
}
bool Robot::AddMinePoint(WallPoint *wp) {
		Sem->wait();
		if(!MinePointExists(wp)) {
			wp->Time = RobotTime;
			WallPoint *del = (WallPoint *)MinePoints->Pop();
			delete del;
			MinePoints->Push(wp);
			//wp->Print();
			//cerr << "WallPoints Size: " << WallPoints->CurrentSize << endl;
			Sem->post();
			return true;
		} else {
			Sem->post();
			return false;
		}
}


bool Robot::AddWallPoint(WallPoint *wp) {
		Sem->wait();
		if(!WallPointExists(wp)) {
			wp->Time = RobotTime;

			WallPoint *del = (WallPoint *)WallPoints->Pop();
			delete del;

			WallPoints->Push(wp);
			//wp->Print();
			//cerr << "WallPoints Size: " << WallPoints->CurrentSize << endl;
			Sem->post();
			return true;
		} else {
			delete wp;
			Sem->post();
			return false;
		}
}

void Robot::Exit() { }
void Robot::Dead() { Playing = false; }
void Robot::SetOption(int Option, double Value) {

	cerr << "RobotOption " << Option << " " << Value << endl;

}
void Robot::GameStarts() { Playing = true; }
void Robot::GameFinished() { Playing = false; }

void Robot::SetSendingCoords(double ReportCoords) {
	//cerr << "Getting coods? " << ReportCoords << endl;
}
void Robot::SetDebug(double Debug) {}

void Robot::SetName(string *_Name) {
	if(Name) delete Name;
	Name = _Name;
}

void Robot::SetColor(string *_Color) {
	if(Color) delete Color;
	Color = _Color;
}

string * Robot::GetColor() {
	return Color;
}

string * Robot::GetName() {
	return Name;
}
