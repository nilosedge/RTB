#include "MessageHandler.h"

MessageHandler::MessageHandler(Robot *_Rob, GunnerThread *_Gunner, MoverThread *_Mover, RadarerThread *_Radarer, RobotGoverner *_Governer) {

	Sem = new Semaphore(1);
	Rob = _Rob;
	Gunner = _Gunner;
	Mover = _Mover;
	Radarer = _Radarer;
	Governer = _Governer;
	TimeToQuit = false;
	fout = new ofstream("/tmp/outfile");
}

void MessageHandler::run() {
	// Wait for other threads to get started
	sleep(1);
	cout << "RobotOption " << USE_NON_BLOCKING << " " << 0 << endl;
	cout << "RobotOption " << SEND_ROTATION_REACHED << " " << 2 << endl;

	while(true) {
		cin >> Message;
		*fout << Message;
		ParseMessage();
		if(TimeToQuit) {
			//clean up and exit;
			return;
		}
		usleep(5);
	}
}

void MessageHandler::ParseMessage() {


	if(!Message.compare("Initialize")) {
		int FirstArg = 0;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		string *Name;
		if(FirstArg == 1) {
			Rob->Init();
			Gunner->Init();
			Mover->Init();
			Radarer->Init();
			Governer->Init();
			Name = Rob->GetName();
			cout << "Name " << *Name << endl;
			string *Color = Rob->GetColor();
			cout << "Colour " << *Color << endl;
		}
	} else if(!Message.compare("YourName")) {
		string FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		Rob->SetName(new string(FirstArg));
	} else if(!Message.compare("YourColour")) {
		string FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		Rob->SetColor(new string(FirstArg));
	} else if(!Message.compare("GameOption")) {
		int FirstArg;
		double SecondArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		cin >> SecondArg;
		*fout << " " << SecondArg;

		switch(FirstArg) {
			case(ROBOT_MAX_ROTATE):
				Mover->SetMaxRotate(SecondArg);
				break;
			case(ROBOT_CANNON_MAX_ROTATE):
				Gunner->SetMaxRotate(SecondArg);
				break;
			case(ROBOT_RADAR_MAX_ROTATE):
				Radarer->SetMaxRotate(SecondArg);
				break;
			case(ROBOT_MAX_ACCELERATION):
				Mover->SetMaxAccel(SecondArg);
				break;
			case(ROBOT_MIN_ACCELERATION):
				Mover->SetMinAccel(SecondArg);
				break;
			case(ROBOT_START_ENERGY):
				Governer->SetStartEnergy(SecondArg);
				break;
			case(ROBOT_MAX_ENERGY):
				Governer->SetMaxEnergy(SecondArg);
				break;
			case(ROBOT_ENERGY_LEVELS):
				Governer->SetEnergyLevels(SecondArg);
				break;
			case(SHOT_SPEED):
				Gunner->SetShotSpeed(SecondArg);
				break;
			case(SHOT_MIN_ENERGY):
				Gunner->SetShotMinEnergy(SecondArg);
				break;
			case(SHOT_MAX_ENERGY):
				Gunner->SetShotMaxEnergy(SecondArg);
				break;
			case(SHOT_ENERGY_INCREASE_SPEED):
				Gunner->SetEnergyIncreaseSpeed(SecondArg);
				break;
			case(TIMEOUT):
				Governer->SetTimeout(SecondArg);
				break;
			case(DEBUG_LEVEL):
				Governer->SetDebug(SecondArg);
				Gunner->SetDebug(SecondArg);
				Radarer->SetDebug(SecondArg);
				Mover->SetDebug(SecondArg);
				Rob->SetDebug(SecondArg);
				break;
			case(SEND_ROBOT_COORDINATES):
				Rob->SetSendingCoords(SecondArg);
				break;

		}

	} else if(!Message.compare("GameStarts")) {
		Rob->GameStarts();
	} else if(!Message.compare("Radar")) {
		long double FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		int SecondArg;
		cin >> SecondArg;
		*fout << " " << SecondArg;
		long double ThirdArg;
		cin >> ThirdArg;
		*fout << " " << ThirdArg;

		if(SecondArg == ROBOT || SecondArg == SHOT || SecondArg == WALL || SecondArg == COOKIE || SecondArg == MINE) {
			Mover->Radar(FirstArg, SecondArg, ThirdArg);
		}

		Radarer->UpdateRadar(FirstArg, SecondArg, ThirdArg);

	} else if(!Message.compare("Info")) {
		long double FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		long double SecondArg;
		cin >> SecondArg;
		*fout << " " << SecondArg;
		long double ThirdArg;
		cin >> ThirdArg;
		*fout << " " << ThirdArg;
		Rob->Info(FirstArg);
		Mover->Info(SecondArg);
		Gunner->Info(ThirdArg);
	} else if(!Message.compare("Coordinates")) {
		long double FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		long double SecondArg;
		cin >> SecondArg;
		*fout << " " << SecondArg;
		long double ThirdArg;
		cin >> ThirdArg;	
		*fout << " " << ThirdArg;
		Mover->SetCoods(FirstArg, SecondArg, ThirdArg);
	} else if(!Message.compare("RobotInfo")) {
		long double FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		int SecondArg;
		cin >> SecondArg;
		*fout << " " << SecondArg;
		Gunner->EnemyInfo(FirstArg, SecondArg);
	} else if(!Message.compare("RotationReached")) {
		int FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		if(FirstArg & 1) Mover->RotationReached();
		if(FirstArg & 2) Gunner->RotationReached();
		if(FirstArg & 4) Radarer->RotationReached();
		Rob->RotationReached(FirstArg);
	} else if(!Message.compare("Energy")) {
		long double FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		Rob->SetEnergyLevel(FirstArg);
	} else if(!Message.compare("RobotsLeft")) {
		int FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		Rob->RobotsLeft(FirstArg);
	} else if(!Message.compare("Collision")) {
		int FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		long double SecondArg;
		cin >> SecondArg;
		*fout << " " << SecondArg;

		Mover->Collision(FirstArg, SecondArg);
	} else if(!Message.compare("Warning")) {
		int FirstArg;
		cin >> FirstArg;
		*fout << " " << FirstArg;
		string SecondArg;
		cin >> SecondArg;
		*fout << " " << SecondArg;
		// Something to get the rest of the line
		Governer->Warning(FirstArg, SecondArg);
	} else if(!Message.compare("Dead")) {
		Rob->Dead();
	} else if(!Message.compare("GameFinishes")) {
		Rob->GameFinished();
	} else if(!Message.compare("ExitRobot")) {
		Rob->Exit();
		TimeToQuit = true;
	} else {
		cerr << "Unknown message: " << Message << endl;
	}
	*fout << endl;
}
