#ifndef RadarerThread_h_
#define RadarerThread_h_

#include <cc++/thread.h>
#include <iostream>
#include "Robot.h"

	using namespace std;
	using namespace ost;

	class RadarerThread : public Thread {

		public:
			RadarerThread(Robot *_Rob);
			~RadarerThread(){

			};

			void Init();
	
			void RotationReached();
			void SetMaxRotate(long double MaxRotate);
			void SetDebug(long double Debug);
			void UpdateRadar(long double Distance, int ObjectType, long double Angle);
			void Exit();

			virtual void run();

		private:

			void RotateRadar(long double RotateAmount);
			Semaphore *Sem;
			Robot *Rob;
			bool Rotating;
			bool Exiting;
			long double RadarDistance;
			int RadarObjectType;
			long double RadarAngle;
			long double MaxRotateSpeed;
	};

#endif
