#ifndef Queue_h_
#define Queue_h_

#include <iostream>
#include <cc++/thread.h>
#include "Node.h"

	using namespace std;
	using namespace ost;

	class Queue {

		public:

			Queue();
			~Queue();
			Queue(int size);
			bool Push(long long data);
			bool Push(Node *);
			Node * Pop();
			bool IsFull();
			bool IsEmpty();
			void Print();
			Node *Head, *Tail;
			volatile int CurrentSize;

		protected:
			int MaxSize;
			Semaphore *Sem;

	};

#endif
