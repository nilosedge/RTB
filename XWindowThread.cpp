#include "XWindowThread.h"

XWindowThread::XWindowThread(Robot *_Rob) {
	height = 610;
	width = 610;
	shift = 300;
	scale = 10;
	Rob = _Rob;
}


void XWindowThread::run() {

	dis = XOpenDisplay(NULL); //TODO Check to see if this returns 0...
	window = XCreateSimpleWindow(dis, RootWindow(dis, 0), 0, 0, width+2, height+2, 0, BlackPixel (dis, 0), BlackPixel(dis, 0));
	win = XCreatePixmap(dis, window, width+2, height+2, DefaultDepth(dis, 0));
	XSelectInput(dis, window, ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask);
	XMapWindow(dis, window);
	colormap = DefaultColormap(dis, 0);
	drawcx = XCreateGC(dis, win, 0, 0);


	XParseColor(dis, colormap, "#000000", &Black);
	XAllocColor(dis, colormap, &Black);

	XParseColor(dis, colormap, "#FFFFFF", &White);
	XAllocColor(dis, colormap, &White);
	XParseColor(dis, colormap, "#00FF00", &Green);
	XAllocColor(dis, colormap, &Green);
	XParseColor(dis, colormap, "#FF0000", &Red);
	XAllocColor(dis, colormap, &Red);

	string *RColor = Rob->GetColor();
	string C(RColor->c_str());

	string Pound("#");
	Pound.append(C);
	cerr << Pound << endl;
	XParseColor(dis, colormap, Pound.c_str(), &RobotC);
	XAllocColor(dis, colormap, &RobotC);

	XSetForeground(dis, drawcx, Black.pixel);
	XFillRectangle(dis, win, drawcx, 0, 0, width, height);



	Queue *q;

	while (1)  {
		XCheckWindowEvent(dis, win, 0, &watch);
		switch  (report.type) {
			case DestroyNotify:
				return;
				break;
		}

		XSetForeground(dis, drawcx, Black.pixel);
		XFillRectangle(dis, win, drawcx, 0, 0, width, height);

		XSetForeground(dis, drawcx, RobotC.pixel);
		WallPoint *wp = Rob->GetLocation();	

		if(wp != NULL) {
			XDrawRectangle(dis, win, drawcx, (int)((wp->X*scale)+shift), (int)((wp->Y*(-scale))+shift), 1, 1);
			delete wp;
		} else {
			cerr << "WP was NULL: Check for RTB sending Coords to Robots." << endl;
		}

		XSetForeground(dis, drawcx, White.pixel);
		q = Rob->GetWallPoints();
		for(WallPoint *Work = (WallPoint *)q->Head; Work != NULL; Work = (WallPoint *)Work->Next) {
			XDrawRectangle(dis, win, drawcx, (int)((Work->X*scale)+shift), (int)((Work->Y*(-scale))+shift), 1, 1);
		}
		if(q != NULL) { delete q; q = NULL; }

		XSetForeground(dis, drawcx, Green.pixel);
		q = Rob->GetCookiePoints();
		for(WallPoint *Work = (WallPoint *)q->Head; Work != NULL; Work = (WallPoint *)Work->Next) {
			XDrawRectangle(dis, win, drawcx, (int)((Work->X*scale)+shift), (int)((Work->Y*(-scale))+shift), 1, 1);
		}
		if(q != NULL) { delete q; q = NULL; }

		XSetForeground(dis, drawcx, Red.pixel);
		q = Rob->GetMinePoints();
		for(WallPoint *Work = (WallPoint *)q->Head; Work != NULL; Work = (WallPoint *)Work->Next) {
			XDrawRectangle(dis, win, drawcx, (int)((Work->X*scale)+shift), (int)((Work->Y*(-scale))+shift), 1, 1);
		}
		if(q != NULL) { delete q; q = NULL; }

		XCopyArea(dis, win, window, drawcx, 0, 0, width, height, 0, 0);
		XFlush(dis);
		usleep(10000);
	}

	return;


}
