#include "GunnerThread.h"

GunnerThread::GunnerThread(Robot *_Rob) {
	Sem = new Semaphore(1);
	Rob = _Rob;
}

void GunnerThread::Info(long double GunAngle) {

}

void GunnerThread::EnemyInfo(long double Level, int Team) {

}

void GunnerThread::RotationReached() {

}

void GunnerThread::Radar(long double Distance, int ObjectType, long double Angle) {


}

void GunnerThread::Init() {

}

void GunnerThread::SetMaxRotate(long double MaxRotate) { }
void GunnerThread::SetShotSpeed(long double ShotSpeed) { }
void GunnerThread::SetShotMinEnergy(long double ShotMinEnergy) { }
void GunnerThread::SetShotMaxEnergy(long double ShotMaxEnergy) { }
void GunnerThread::SetEnergyIncreaseSpeed(long double IncreaseSpeed) { }

void GunnerThread::SetDebug(long double Debug) {}


void GunnerThread::run() {

	while(true) {
		sleep(1);
	}
}
