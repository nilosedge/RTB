#include "MessageHandler.h"
#include "GunnerThread.h"
#include "MoverThread.h"
#include "RadarerThread.h"
#include "RobotGoverner.h"
#include "XWindowThread.h"
#include <string>

int main() {
	
	Robot *bot = new Robot(new string("FF99CC"), new string("Sprike"));

	XWindowThread *w = new XWindowThread(bot);

	GunnerThread *g = new GunnerThread(bot);
	RadarerThread *r = new RadarerThread(bot);
	RobotGoverner *gv = new RobotGoverner(bot);
	MoverThread *m = new MoverThread(bot);

	g->start();
	m->start();
	r->start();
	gv->start();

	w->start();

	MessageHandler *mes = new MessageHandler(bot, g, m, r, gv);
	mes->start();

	while(1) {
		sleep(1);
	}

}
