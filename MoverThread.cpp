#include "MoverThread.h"

MoverThread::MoverThread(Robot *_Rob) {
	Rob = _Rob;
	Sem = new Semaphore(1);

	RadarDistance = 0;
	RadarObjectType = 0;
	RadarAngle = 0;

	Exiting = false;
	MaxRotateSpeed = 0;
	DebugLevel = 0;
	FirstRadar = true;
	Rotating = false;
	MaxAccelSpeed = 100;
	MinAccelSpeed = -100;
	Counter = 0;
	scale = 1;
}

void MoverThread::Exit() {
	Exiting = true;
}

void MoverThread::Init() {


	Rob->SetLocation(0, 0);
}

void MoverThread::Info(long double CurrentSpeed) {


}

void MoverThread::Collision(int ObjectType, long double RealitiveAngle) {

}

void MoverThread::SetCoods(long double X, long double Y, long double Angle) {
	//cerr << "Mover-> Coods: X: " << X << " Y: " << Y << " Angle: " << Angle << endl;
	Sem->wait();
	Direction = Angle;
	WallPoint *wp;
	//cout << "DebugLine " << (-Angle - M_PI_2l) << " 1 " << (-Angle - M_PI_2l) << " 6" << endl;
	//cout << "DebugLine " << (-Angle - 2) << " 1 " << (-Angle - 2) << " 6" << endl;
	//cout << "DebugLine " << -Angle << " 1 " << -Angle << " 6" << endl;
	if(RadarObjectType == WALL || RadarObjectType == COOKIE || RadarObjectType == MINE) {
		wp = new WallPoint((X + (RadarDistance * cos(((RadarAngle) + Angle)))), (Y + (RadarDistance * sin((RadarAngle) + Angle))));

		//wp = new WallPoint((int)((X*scale) + ((RadarDistance*scale) * cos(RadarAngle + Angle))), (int)((Y*scale) + ((RadarDistance*scale) * sin(RadarAngle + Angle))));
		//wp = new WallPoint((int)((X*scale) + ((RadarDistance*scale) * cos(RadarAngle))), (int)((Y*scale) + ((RadarDistance*scale) * sin(RadarAngle))));
		//wp = new WallPoint((int)(X + (RadarDistance * cos(RadarAngle + Angle))), (int)(Y + (RadarDistance * sin(RadarAngle + Angle))));
		//wp = new WallPoint(X, Y);
	}
	if(RadarObjectType == WALL)	Rob->AddWallPoint(wp);
	if(RadarObjectType == COOKIE)	Rob->AddCookiePoint(wp);
	if(RadarObjectType == MINE)	Rob->AddMinePoint(wp);
	Rob->SetLocation(X, Y);
	Sem->post();

}

void MoverThread::RotationReached() {
	Rotating = false;
}

void MoverThread::SetMaxRotate(double MaxRotate) {
	MaxRotateSpeed = MaxRotate;
	//cerr << "MaxRobotRotateSpeed: " << MaxRotate << endl;
}

void MoverThread::SetDebug(double Debug) {
	DebugLevel = Debug;
	//cerr << "DebugLevel: " << Debug << endl;
}
void MoverThread::SetMaxAccel(double MaxAccel) {
	MaxAccelSpeed = MaxAccel;
	//cerr << "MaxAccelSpeed: " << MaxAccelSpeed << endl;
}
void MoverThread::SetMinAccel(double MinAccel) {
	MinAccelSpeed = MinAccel;
	//cerr << "MinAccelSpeed: " << MinAccelSpeed << endl;
}

void MoverThread::TurnRobot() {

   if(!Rotating) {
      Rotating = true;
      //if((WorkingAngle + RotateAmount) > (M_PI * 2)) {
      //   WorkingAngle = (WorkingAngle + RotateAmount) - (M_PI * 2);
      //} else {
      //}
      cout << "RotateAmount 1 0.5 " << (Direction + 4) << endl;
      //cout << "RotateAmount 1 2 " << WorkingAngle + 1 << endl;
      //cerr << "Rotating to: " << WorkingAngle << endl;
   }

}

void MoverThread::AccelerateRobot(long double AccelAmount) {
	
	if(Counter == 0) {
		cout << "Accelerate " << MaxAccelSpeed << endl;
		cerr << "Accelerate " << MaxAccelSpeed << endl;
		Counter++;
	}

}

void MoverThread::Radar(long double Distance, int ObjectType, long double Angle) {
	Sem->wait();
	cerr << "Mover-> Radar: Dist: " << RadarDistance << " Type: " << RadarObjectType << " Angle: " << Angle << endl;
	RadarDistance = Distance;
	RadarObjectType = ObjectType;
	RadarAngle = Angle;

	WallPoint *wp;
	if(RadarObjectType == WALL || RadarObjectType == COOKIE || RadarObjectType == MINE) {
		wp = new WallPoint(((RadarDistance * cos(((RadarAngle) + Angle)))), ((RadarDistance * sin((RadarAngle) + Angle))));
	}
	if(RadarObjectType == WALL)	Rob->AddWallPoint(wp);
	if(RadarObjectType == COOKIE)	Rob->AddCookiePoint(wp);
	if(RadarObjectType == MINE)	Rob->AddMinePoint(wp);
	
	Sem->post();
}

void MoverThread::run() {

	while(!Exiting) {
		while(Rob->Playing) {
			if(Rob->State == Rob->MAPPING) {
				TurnRobot();
				AccelerateRobot(MaxAccelSpeed);
			}
			usleep(5);
		}
		while(!Rob->Playing) {
			usleep(5);
		}
	}
	usleep(5);
}
