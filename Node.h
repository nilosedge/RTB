#ifndef Node_h_
#define Node_h_

#include <iostream>

	using namespace std;

	class Node {
		public:

			Node(int id = 0);
			void Print();
			~Node();

		public:
			int id;
			Node *Next, *Prev;
	};

#endif
